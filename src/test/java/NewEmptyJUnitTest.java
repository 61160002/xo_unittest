/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

import com.mycompany.oxswing.Player;
import org.junit.jupiter.api.AfterEach;
import org.junit.jupiter.api.AfterAll;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.*;
import com.mycompany.oxswing.Table;


public class NewEmptyJUnitTest {
    
    public NewEmptyJUnitTest() {
    }
    
    @BeforeAll
    public static void setUpClass() {
    }
    
    @AfterAll
    public static void tearDownClass() {
    }
    
    @BeforeEach
    public void setUp() {
    }
    
    @AfterEach
    public void tearDown() {
    }
    @Test
    public void testRow1Win() {
        Player o = new Player('o');
        Player x = new Player('x');
        Table table = new Table(o, x);
        table.setRowCol(1, 1);
        table.setRowCol(1, 2);
        table.setRowCol(1, 3);
        assertEquals(true, table.checkWin());   
    }
    
     @Test 
     public void testRow2Win() {
        Player o = new Player('o');
        Player x = new Player('x');
        Table table = new Table(o, x);
        table.setRowCol(2, 1);
        table.setRowCol(2, 2);
        table.setRowCol(2, 3);
        assertEquals(true, table.checkWin());   
    }
     
    @Test 
    public void testRow3Win() {
        Player o = new Player('o');
        Player x = new Player('x');
        Table table = new Table(o, x);
        table.setRowCol(3, 1);
        table.setRowCol(3, 2);
        table.setRowCol(3, 3);
        assertEquals(true, table.checkWin());   
    }
    @Test
    public void testCol1Win() {
        Player o = new Player('o');
        Player x = new Player('x');
        Table table = new Table(o, x);
        table.setRowCol(1, 1);
        table.setRowCol(2, 1);
        table.setRowCol(3, 1);
        assertEquals(true, table.checkWin());   
    }
    
    @Test
    public void testCol3Win() {
        Player o = new Player('o');
        Player x = new Player('x');
        Table table = new Table(o, x);
        table.setRowCol(1, 3);
        table.setRowCol(2, 3);
        table.setRowCol(3, 3);
        assertEquals(true, table.checkWin());   
    }
    
    @Test
    public void testCol2Win() {
        Player o = new Player('o');
        Player x = new Player('x');
        Table table = new Table(o, x);
        table.setRowCol(1, 2);
        table.setRowCol(2, 2);
        table.setRowCol(3, 2);
        assertEquals(true, table.checkWin());   
    }
    @Test
    public void testX1Win() {
        Player o = new Player('o');
        Player x = new Player('x');
        Table table = new Table(o, x);
        table.setRowCol(1, 1);
        table.setRowCol(2, 2);
        table.setRowCol(3, 3);
        assertEquals(true, table.checkWin());   
    }
    @Test
    public void testX2Win() {
        Player o = new Player('o');
        Player x = new Player('x');
        Table table = new Table(o, x);
        table.setRowCol(1, 3);
        table.setRowCol(2, 2);
        table.setRowCol(3, 1);
        assertEquals(true, table.checkWin());   
    }
    public void testSwitchPlayer() { // Test Case
        Player o = new Player('o');
        Player x = new Player('x');
        Table table = new Table(o, x);
        table.switchPlayer();
        assertEquals('x', table.getCurrentPlayer().getName());
    }
    
    @Test
    public void testDraw() {
        Player o = new Player('o');
        Player x = new Player('x');
        Table table = new Table(x, o);
        table.setRowCol(1, 1);
        table.switchPlayer();
        table.setRowCol(2, 2);
        table.switchPlayer();
        table.setRowCol(1, 2);
        table.switchPlayer();
        table.setRowCol(1, 3);
        table.switchPlayer();
        table.setRowCol(3, 1);
        table.switchPlayer();
        table.setRowCol(2, 1);
        table.switchPlayer();
        table.setRowCol(2, 3);
        table.switchPlayer();
        table.setRowCol(3, 2);
        table.switchPlayer();
        table.setRowCol(3, 3);
        assertEquals(true, table.checkWin());   
    }
}
